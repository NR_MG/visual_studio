﻿#include <iostream>

//Вызывает функцию std::cout
void print()
{
    std::cout << "Hello World!\n";
}

int main()
{
    print();

    int x = 100;
    int y = x + 100;
    int mult = x * y;

    std::cout << mult;
}

